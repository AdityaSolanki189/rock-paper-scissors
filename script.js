$(".div-four").addClass("hide");

console.log("script.js loaded");

let player_score = 0;
let computer_score = 0;

let pattern = ["rock", "paper", "scissors"];

$("#start").click(function() {
    $("#start").addClass("hide");
    $("#reset").removeClass("hide");
    $(".div-four").removeClass("hide");
});

$("#reset").click(function() {
    $("#reset").addClass("hide");
    $("#start").removeClass("hide");
    $(".div-four").addClass("hide");
    player_score = 0;
    computer_score = 0;
    $("#player-score").text(player_score);
    $("#computer-score").text(computer_score);
});

$(".rock").click(function() {
    
    let computer_choice = pattern[Math.floor(Math.random() * 3)];
    console.log(computer_choice);

    if (computer_choice == "rock") {
        $("#result").text("It's a tie!");
    } else if (computer_choice == "paper") {
        $("#result").text("Computer wins!");
        computer_score++;
        $("#computer-score").text(computer_score);
    } else {
        $("#result").text("You win!");
        player_score++;
        $("#player-score").text(player_score);
    }

    $("#player-image").attr("src", "assets/rock.png");
    $("#comp-image").attr("src", "assets/" + computer_choice + ".png");
    $(".div-three").addClass("shake");

    setTimeout(function() {
        
        $(".div-three").removeClass("shake");
        $("#comp-image").attr("src", "assets/rock.png");

        $("#result").text("Choose your weapon!");
    }, 1500);

});

$(".paper").click(function() {
    
    let computer_choice = pattern[Math.floor(Math.random() * 3)];
    console.log(computer_choice);
    
    if (computer_choice == "paper") {
        $("#result").text("It's a tie!");
    } else if (computer_choice == "scissors") {
        $("#result").text("Computer wins!");
        computer_score++;
        $("#computer-score").text(computer_score);
    } else {
        $("#result").text("You win!");
        player_score++;
        $("#player-score").text(player_score);
    }

    $("#player-image").attr("src", "assets/paper.png");
    $("#comp-image").attr("src", "assets/" + computer_choice + ".png");
    $(".div-three").addClass("shake");

    setTimeout(function() {
        $(".div-three").removeClass("shake");
        $("#comp-image").attr("src", "assets/rock.png");
        $("#player-image").attr("src", "assets/rock.png");

        $("#result").text("Choose your weapon!");
    }, 1500);

});

$(".scissor").click(function() {
    
    let computer_choice = pattern[Math.floor(Math.random() * 3)];
    console.log(computer_choice);
    
    if (computer_choice == "scissors") {
        $("#result").text("It's a tie!");
    } else if (computer_choice == "rock") {
        $("#result").text("Computer wins!");
        computer_score++;
        $("#computer-score").text(computer_score);
    } else {
        $("#result").text("You win!");
        player_score++;
        $("#player-score").text(player_score);
    }

    $("#player-image").attr("src", "assets/scissors.png");
    $("#comp-image").attr("src", "assets/" + computer_choice + ".png");
    $(".div-three").addClass("shake");

    setTimeout(function() {
        $(".div-three").removeClass("shake");
        $("#comp-image").attr("src", "assets/rock.png");
        $("#player-image").attr("src", "assets/rock.png");

        $("#result").text("Choose your weapon!");
    }, 1500);

});